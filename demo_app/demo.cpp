#pragma once
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <conio.h>
#include <frame_source.h>
#include "demo.h"
#include <camshift_class.h>
#include "KalmanTracker.h"

using namespace std;
using namespace cv;

void main(int argc, char *argv[])
{
	FaceTracker* tracker;
	KalmanTracker* kalman = new KalmanTracker;
	//FaceTracker* tracker_cal;*	
	if (argc == 1)
	{
		tracker = new FaceTracker(new CameraSource);
	}
	else if (argc == 2)
	{
		tracker = new FaceTracker(new FileSource("d:/itlab/face3d.mp4"));
		//tracker_cal = new FaceTracker(new FileSource);
	}

	tracker->Calibrate();
	if (argc == 2)
//		tracker_cal->Calibrate();
//	Point oldpoint = tracker->Position().center;
//	Point oldpoint_cal = tracker_cal->Position().center;//cal
	namedWindow( "CamShift", 0 );
	namedWindow( "CamShift + Kalman", 0 );
	moveWindow ("CamShift",20,90);
	moveWindow ("CamShift + Kalman",680,90);
	resizeWindow("CamShift",640,480);
	resizeWindow("CamShift + Kalman",640,480);
	for (;;)
	{
		if (tracker->image.empty())
			break;
		cv::Mat im = tracker->image.clone();
		cv::Mat imm = im.clone();
		cv::RotatedRect rr = tracker->OutputPosition();
		cv::Point rrrr = kalman->Currect(rr.center);
		cv::RotatedRect rrr = rr;
		rrr.center = rrrr;
		ellipse(im, rr, Scalar(0,0,255), 3, CV_AA );
		ellipse(imm, rrr, Scalar(0,0,255), 3, CV_AA );
		imshow("CamShift", im);
		imshow("CamShift + Kalman", imm);
/*		if (argc == 2)
		{
			//����� ���� � �.�������
		//	ellipse(tracker_cal->image, tracker_cal->Position(), Scalar(0,0,255), 3, CV_AA );//cal
			line(im, rr.center, oldpoint, Scalar(0,0,255), 3, CV_AA );
			line(imm, rrr.center, oldpoint, Scalar(0,0,255), 3, CV_AA );
		//	line(tracker_cal->image, tracker_cal->Position().center, oldpoint, Scalar(0,0,255), 3, CV_AA );//cal
			oldpoint = rr.center;
		//	oldpoint_cal = tracker_cal->Position().center;//cal
		}
*/
		char c = (char)waitKey(10);
		if((c == 13) | ( c == 27 ))
			{
				destroyWindow( "CamShift" );
				destroyWindow("Camshift + Kalman");
				break;
			}
	}
/*	namedWindow ("Camshift",0);
	namedWindow ("Camshift + Calman",0);
	moveWindow ("Camshift + Calman",50,0);
*/
	/*char c = (char)waitKey(10);
			if((c == 'A') | ( c == 'a' ))
			{
				new FaceTracker(new CameraSource);
				tracker.Calibrate();
				for (;;)
				{
					cout<<tracker.Position().center<<endl;
					ellipse (image, trackBox, Scalar(0,0,255),3,CV_AA);
				}
	
			}
			if((c == 'B') | ( c == 'b' ))
			{
				cout << "\nThis is a program that shows differnces between using"
						"CamShift w/ Calman filter and using CamShift w/o Calman filter\n";

			}*/
}