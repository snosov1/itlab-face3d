#include <iostream>
#include <conio.h>
#include <tinythread.h>
#include <MyKalman.h>

using namespace std;

#include "camshift_class.h"

void main()
{
	FaceTracker* tracker;
	tracker->Calibrate();
	/*for (;;)
	{
		cout<<tracker.Position().center<<endl;

	}*/
	Mykalman a;
	float x,y;
	a.SetMeasurement(5,10);
	a.Predict();
	a.Correct();
	x=a.GetElemEstimated_i(0);
	y=a.GetElemEstimated_i(1);
	cout<<x<<"_"<<y;
	getch();
}