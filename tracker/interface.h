#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

class FrameSource 
{
	virtual cv::Mat GetFrame() = 0;
	virtual void NextFrame() = 0;
};


class CameraSource : public FrameSource 
{
	cv::Mat frame;
	cv::VideoCapture cap;
	virtual void NextFrame();
	virtual cv::Mat GetFrame();
};