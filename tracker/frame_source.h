#pragma once
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
//#include <string>

class FrameSource 
{
public:
	virtual cv::Mat GetFrame() = 0;
	virtual void NextFrame() = 0;
};

class CameraSource : public FrameSource 
{
	cv::Mat frame;
	cv::VideoCapture cap;
public:
	CameraSource();
	virtual void NextFrame();
	virtual cv::Mat GetFrame();
};
class FileSource : public FrameSource
{
	cv::Mat frame;
	cv::VideoCapture file;
public:
	//FileSource(){};
	FileSource(const char* str);
	virtual void NextFrame();
	virtual cv::Mat GetFrame();
};


