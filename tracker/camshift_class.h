#pragma once
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "frame_source.h"
#include <memory>
#include <tinythread.h>


#include <iostream>
#include <ctype.h>

class FaceTracker
	{
		std::unique_ptr<FrameSource> fs;
		std::unique_ptr <tthread::thread> t;
		
		bool backprojMode;
		bool selectObject;
		int trackObject;
		bool showHist;
		cv::Point origin;
		cv::Rect selection;
		int vmin, vmax, smin;
		cv::Mat frame, hsv, hue, mask, hist, histimg, backproj;
		bool paused;
		cv::VideoCapture cap;
		cv::Rect trackWindow;
		int hsize;
		float hranges[2];
		const float* phranges;
		static void loop_position(void * aArg);
		cv::RotatedRect rr;
	public:
		FaceTracker(FrameSource* a);
		~FaceTracker();
		static void onMouse( int event, int x, int y, int, void* arg);
		void help();
		void Calibrate();
		cv::RotatedRect Position();
		cv::RotatedRect OutputPosition(); 
		cv::Mat image;
	};