#include "camshift_class.h"

using namespace cv;
using namespace std;
using namespace tthread;

FaceTracker::FaceTracker(FrameSource* a) 
	: phranges(hranges)
	, backprojMode(false)
	, selectObject(false)
	, trackObject(0)
	, showHist(true)
	, vmin(10)
	, vmax(256)
	, smin(30)
	, histimg(Mat::zeros(200, 320, CV_8UC3))
	, paused(false)
	, hsize(16)
	, fs()

	{
		hranges[0] = 0;
		hranges[1] = 180;
		fs.reset(a);
	}

FaceTracker::~FaceTracker() { }

void FaceTracker::onMouse( int event, int x, int y, int, void* arg)
	{
		FaceTracker *self = (FaceTracker*)arg;
		if( self->selectObject )
		{
			self->selection.x = MIN(x, self->origin.x);
			self->selection.y = MIN(y, self->origin.y);
			self->selection.width = std::abs(x - self->origin.x);
			self->selection.height = std::abs(y - self->origin.y);

			self->selection &= Rect(0, 0, self->image.cols, self->image.rows);
		}

		switch( event )
		{
		case CV_EVENT_LBUTTONDOWN:
			self->origin = Point(x,y);
			self->selection = Rect(x,y,0,0);
			self->selectObject = true;
			break;
		case CV_EVENT_LBUTTONUP:
			self->selectObject = false;
			if( self->selection.width > 0 && self->selection.height > 0 )
				self->trackObject = -1;
			break;
		}
	}

void FaceTracker::help()
	{
		cout << "\nThis is a program that shows mean-shift based tracking\n"
				"You select a color objects such as your face and it tracks it.\n";

		cout << "\n\nHot keys: \n"
				"\tEnter or ESC - quit the camera's window\n"
				"To initialize tracking, select the object with mouse\n";

	const char* keys =
	{
		"{1|  | 0 | camera number}"
	};

	}

void FaceTracker::Calibrate()
	{
		help();

		//namedWindow( "Histogram", 0 );
		namedWindow( "CamShift Demo", 0 );
		moveWindow("CamShift Demo",5,5);
		resizeWindow("CamShift Demo",640,650);
		setMouseCallback( "CamShift Demo", onMouse, this );
		createTrackbar( "Vmin", "CamShift Demo", &vmin, 256, 0 );
		createTrackbar( "Vmax", "CamShift Demo", &vmax, 256, 0 );
		createTrackbar( "Smin", "CamShift Demo", &smin, 256, 0 );

		for(;;)
		{
			if( !paused )
			{
				/*cap >> frame;*/
				fs->NextFrame();
				frame = fs->GetFrame();
				if( frame.empty() )
					break;
			}

			frame.copyTo(image);
						if( !paused )
			{
				cvtColor(image, hsv, CV_BGR2HSV);

				if( trackObject )
				{
					int _vmin = vmin, _vmax = vmax;

					inRange(hsv, Scalar(0, smin, MIN(_vmin,_vmax)),
							Scalar(180, 256, MAX(_vmin, _vmax)), mask);
					int ch[] = {0, 0};
					hue.create(hsv.size(), hsv.depth());
					mixChannels(&hsv, 1, &hue, 1, ch, 1);

	 				if( trackObject < 0 )
					{
						Mat roi(hue, selection), maskroi(mask, selection);
						calcHist(&roi, 1, 0, maskroi, hist, 1, &hsize, &phranges);
						normalize(hist, hist, 0, 255, CV_MINMAX);

						trackWindow = selection;
						trackObject = 1;

						histimg = Scalar::all(0);
						int binW = histimg.cols / hsize;
						Mat buf(1, hsize, CV_8UC3);
						for( int i = 0; i < hsize; i++ )
							buf.at<Vec3b>(i) = Vec3b(saturate_cast<uchar>(i*180./hsize), 255, 255);
						cvtColor(buf, buf, CV_HSV2BGR);

						for( int i = 0; i < hsize; i++ )
						{
							int val = saturate_cast<int>(hist.at<float>(i)*histimg.rows/255);
							rectangle( histimg, Point(i*binW,histimg.rows),
									   Point((i+1)*binW,histimg.rows - val),
									   Scalar(buf.at<Vec3b>(i)), -1, 8 );
						}
						
					//calcBackProject(&hue, 1, 0, hist, backproj, &phranges);
					//backproj &= mask;
					//RotatedRect trackBox = CamShift(backproj, trackWindow,
				//						TermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 ));
				if( trackWindow.area() <= 1 )
					{
						int cols = backproj.cols, rows = backproj.rows, r = (MIN(cols, rows) + 5)/6;
						trackWindow = Rect(trackWindow.x - r, trackWindow.y - r,
										   trackWindow.x + r, trackWindow.y + r) &
									  Rect(0, 0, cols, rows);
				}

					if( backprojMode )
						cvtColor( backproj, image, CV_GRAY2BGR );
				//	ellipse( image, trackBox, Scalar(0,0,255), 3, CV_AA );

					}
				}
			else if( trackObject < 0 )
				paused = false;
			if( selectObject && selection.width > 0 && selection.height > 0 )
			{
				Mat roi(image, selection);
				bitwise_not(roi, roi);
			}
			imshow( "CamShift Demo", image );

			imshow( "Histogram", histimg );
			}
			char c = (char)waitKey(10);
			if((c == 13) | ( c == 27 ))
			{
				//paused = !paused;
				destroyWindow( "Histogram" );
				destroyWindow( "CamShift Demo" );
				break;
			}
			/*switch(c)
			{
			case 'b':
				backprojMode = !backprojMode;
				break;
			case 'c':
				trackObject = 0;
				histimg = Scalar::all(0);
				break;
			case 'h':
				showHist = !showHist;
				if( !showHist )
					destroyWindow( "Histogram" );
				else
					namedWindow( "Histogram", 1 );
				break;
			case 'p':
				paused = !paused;
				destroyWindow( "CamShift Demo" );
				break;
			default:
				;
			}*/
		}
	  t.reset(new thread(&FaceTracker::loop_position, this));
	}
	//RotatedRect trackBox;
	RotatedRect FaceTracker::OutputPosition() 
	{
		return rr;
	}
	RotatedRect FaceTracker::Position()
	{
		RotatedRect trackBox;
		//namedWindow( "Web", 0 );
	//	for(;;)
		{
			//cap.open(0);
				//cap >> frame;
				fs->NextFrame();
				frame = fs->GetFrame();
				if( !frame.empty() )
				{
		
			frame.copyTo(image);

				cvtColor(image, hsv, CV_BGR2HSV);
				int _vmin = vmin, _vmax = vmax;

					inRange(hsv, Scalar(0, smin, MIN(_vmin,_vmax)),
							Scalar(180, 256, MAX(_vmin, _vmax)), mask);
					int ch[] = {0, 0};
					hue.create(hsv.size(), hsv.depth());
					mixChannels(&hsv, 1, &hue, 1, ch, 1);

						

				if( trackObject )
				{
					Mat roi(hue, selection), maskroi(mask, selection);
					calcHist(&roi, 1, 0, maskroi, hist, 1, &hsize, &phranges);
					normalize(hist, hist, 0, 255, CV_MINMAX);
				//	backproj = Mat::Mat(
					cvtColor( image,backproj, CV_BGR2GRAY );
					calcBackProject(&hue, 1, 0, hist, backproj, &phranges);
					backproj &= mask;
					trackBox = CamShift(backproj, trackWindow,
										TermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 ));
					//trackBox.center;
					trackWindow.x = trackBox.center.x - trackBox.size.width / 2;
					if ((trackWindow.x <= 0) | (trackWindow.x >= 640))
						trackWindow.x = 640;
					trackWindow.y = trackBox.center.y - trackBox.size.height / 2;
					if ((trackWindow.y <= 0) | (trackWindow.y >= 640))
						trackWindow.y = 480;


				//	imshow( "Web", image );
				}

					//return trackBox;
				}	
			return trackBox;
		}
		//destroyWindow( "Web" );

		//return trackBox;
	}

 void FaceTracker::loop_position(void * aArg)
   { 
	   FaceTracker *Self=(FaceTracker*)aArg;
	   while(1)
        {
          Self->rr=Self->Position();
	    }
    }