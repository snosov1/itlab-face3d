#ifndef _TRACKER_HPP_
#define _TRACKER_HPP_

#include <iostream>

namespace face3d
{
    class Hello
    {
    public:
        void hello(std::ostream &out);
    };
}

#endif /* _TRACKER_HPP_ */
