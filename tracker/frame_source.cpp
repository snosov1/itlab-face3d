#include "frame_source.h"

using namespace cv;

//from Camera

CameraSource::CameraSource()
{
	cap.open(0);
}

void CameraSource::NextFrame() 
	{
		cap >> frame;
	}
cv::Mat CameraSource::GetFrame() 
	{
		return frame;
	}

//from File
FileSource::FileSource(const char* str)
{
	file.open(str);
}
void FileSource::NextFrame() 
	{
		file >> frame;
	}
cv::Mat FileSource::GetFrame() 
	{
		return frame;
	}
