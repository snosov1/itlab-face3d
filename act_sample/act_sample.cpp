#include <iostream>
#include <tinythread.h>

using namespace std;
using namespace tthread;


class TActive{
private:
	int x;
	thread *t;
  static void SetX(void * aArg);
    
public:
   int GetX();
   TActive();
   ~TActive();
};

 TActive::TActive()
  {
 	  t= new thread(&TActive::SetX, this);
  }

 void TActive::SetX(void * aArg)
   { 
	   TActive *Self=(TActive*)aArg;
	   for(;;)
  {
  
	   Self->x=rand();
	   }
    }

 int TActive::GetX()
  { 
	  return x;
  }
  
 TActive::~TActive()
  {
	  delete t;
  }
 

int main()
{
  TActive a;
  for(;;)
  {
  cout << a.GetX() << endl;
  }
}