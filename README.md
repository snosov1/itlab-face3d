Meetings
========

# [2013-01-22 Wed] #

## Agenda ##

1. Active tracker
2. Demo application
    * video
    * FileSource
3. KalmanTracker

## Minutes ##

1. **AR Sasha** rewrite Dima's tracker to be an active object similar
   to act_sample
2. **AR Dima** Implement a demo application. Features include:
    * run with input from camera or file
    * draw tracking ellipse and optional track of its center
    * two video-streams side-by-side for comparing orignal CAMSHIFT
      and Kalman filter enhanced
3. **AR Vlad** Once again - push kalman lib

# [2012-12-13 Wed] #

## Agenda ##

1. Status check

## Minutes ##

1. Guys are encouraged to work on Monday from the university, no
   face-to-face assist will be granted
2. **AR Dima** implement FileSource as another FrameSource interface
   implementation. No fancy video formats, just multiple frames in a
   directory
3. **AR Sasha** refactor Dima's implementation of tracker to be an
   active object
4. **AR Vlad** push kalman lib and sample app at last

# [2012-12-05 Wed] #

## Agenda ##

1. Discipline
2. Status check
3. Broken build
4. .h and .cpp, namespaces
5. FrameSource interface

## Minutes ##

1. Another meeting on Monday, Sveta will be available from 11:20 to
   16:00, Sergei will be available during the day via e-mail or chat
2. **AR Dima** make sample work
3. **AR Dima** introduce FrameSource interface and CameraSource
   implementation
4. **AR Vlad** push kalman lib and sample app
5. **AR Sasha** push sample
6. **AR Sasha** implement FileSource, ask Dima for explanation

## Apeendix ##

    class FrameSource {
        virtual cv::Mat GetFrame() = 0;
        virtual void NextFrame() = 0;
    };

    class CameraSource : public FrameSource {
        cv::Mat mat;
        cv::VideoCapture cap;
        virtual void NextFrame() {
            cap >> mat;
        }
        virtual cv::Mat GetFrame() {
            return mat;
        }
    }

# [2012-11-28 Wed] #

## Agenda ##

1. Status check

## Minutes ##

1. Additional meetings are scheduled on Mondays, 3 and 10 at 11:20
2. **AR Vlad** send out a status report
3. **AR Sasha** complete active class
4. **AR Dima** finalize class implementation
5. **AR Vlad** implement KalmanFilter1D functionality and push to bitbucket

# [2012-11-21 Wed] #

## Agenda ##

1. Status check

## Minutes ##

1. **AR Sasha** rewrite "active" class to have thread object as a member
2. **AR Dima** bring all global variables inside of class definition
3. **AR Vlad** implement KalmanFilter1D functionality and push to bitbucket

# [2012-11-14 Wed] #

## Agenda ##

1. Status check

## Minutes ##

1. **AR Sasha** push tinythreads to Bitbucket
2. **AR Sasha** sample "active" class
3. **AR Dima** go on implementing camshift functionality, should have
compiling version to next meeting
4. **AR Vlad** send report with current status
5. **AR Vlad** write classes KalmanFilter1D and KalmanFilter2D: methods filter(const Point &p)


# [2012-11-07 Wed] #

Missed

# [2012-10-31 Wed] #

## Agenda ##

1. Seminar
2. Subjects talk

## Minutes ##

1. **AR Sasha** get [tinythreads](http://tinythreadpp.bitsnbites.eu/) working in our source tree and write a sample
2. **AR Sasha** push library to Bitbucket
3. **AR Dima** Write a class FaceTracker: methods Calibrate() and Position()
4. **AR Vlad** Write classes KalmanFilter1D and KalmanFilter2D: methods filter(const Point &p)

# [2012-10-24 Wed] #

## Agenda ##

1. Checking homework
2. Resolving opens
3. CAMSHIFT
4. TinyThreads
5. KalmanFilter

## Minutes ##

1. **AR Sasha** reinstall VS, build the project, show image
2. **AR guys** use git to clone the project
    * [Git](http://git-scm.com/) Git is intended to be used via
      command line, but if you're not Linux kernel developer, you may
      not feel very comfotable using it that way. So you can download
      the GUI front-end
      [TortoiseGit](http://code.google.com/p/tortoisegit/)
    * git clone
    * git add
    * git commit
    * git push
    * git pull
3. **AR guys** consiously understand what's what in project tree
4. **AR sergei** send threads sample
5. **AR Dima** understand camshift sample and explain to everyone
6. **AR Sasha** get [tinythreads](http://tinythreadpp.bitsnbites.eu/) working in our source tree and write a sample
7. **AR Vlad** understand kalman sample and explain to everyone

# [2012-10-17 Wed] #

## Agenda ##

1. Acquaintance
2. Time
3. Reply-to-all
4. Project overview
5. [Bitbucket](http://bitbucket.org), invitations
6. Required SW
    * [CMake](http://www.cmake.org/)
    * [Git](http://git-scm.com/)
    * [TortoiseGit](http://code.google.com/p/tortoisegit/)
    * [OpenCV](http://opencv.org/)

## Minutes ##

1. As we found out on the meeting, everybody fluently speaks English,
   so I'm sure you'll have no problems with meeting minutes in
   English. Few words about what it is.
       * *Agenda* is something like a "plan" for the meeting. It lists topics
       which are planned for discussion.
       * *Meeting minutes* is something like a "result" of the meeting. It
       lists tasks for each person to be done and decisions made.
       * A task for someone is called *AR* (action required), so if I write "AR
       Sergei: send meeting minutes" it means that I have to take action -
       send the minutes.

2. **AR guys** I've sent everybody an invitation on
[Bitbucket](http://bitbucket.org), please, sign up.

3. **AR guys** install the required software
    * [CMake](http://www.cmake.org/)
    * [Git](http://git-scm.com/)
    * Git is intended to be used via command line, but if you're not
      Linux kernel developer, you may not feel very comfotable using it
      that way. So you can download the GUI front-end
      [TortoiseGit](http://code.google.com/p/tortoisegit/)
    * [OpenCV](http://opencv.org/)

4. **AR guys** build and run the project
    * clone the [project](https://bitbucket.org/snosov1/itlab-face3d/)
    * open run-cmake-gui.bat in an editor and change OpenCV\_DIR and
      CMake\_Path to your local values
    * run run-cmake-gui.bat, it should open cmake-gui
    * enter your project directory in field *Source
      Directory* (e.g. c:/projects/itlab-face3d) and something like
      c:/projects/itlab-face3d/build in the *Build Directory* field
    * press Configure, choose Visual Studio 2010
    * press Generate
    * after that you will find the solution file in
      c:/projects/itlab-face3d/build - open it in VS.

5. **AR guys** use OpenCV to load and show the image according this
   [tutorial](http://docs.opencv.org/doc/tutorials/introduction/display_image/display_image.html)
