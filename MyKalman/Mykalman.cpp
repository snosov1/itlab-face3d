#include "MyKalman.h"


Mykalman::Mykalman(void)
{
	KalmanFilter KF1(2,2,0);
	KF = KF1;

	Mat_<float> state(2,1);
	this->state = state;
	this->measurement.setTo(Scalar(0));
	Mat_<float> estimated(2,1);
	this->estimated = estimated;
	Mat processNoise(2, 1, CV_32F);
	this->processNoise = processNoise;
	Mat_<float> measurement(2,1);
	this->measurement = measurement;
	this->measurement.setTo(Scalar(0));
	this->KF.transitionMatrix = *(Mat_<float>(2, 2) << 1,0,   0,1);

	//setIdentity(measurement, Scalar::all(1e-4));
	setIdentity(KF.measurementMatrix, cvRealScalar(1) );
	setIdentity(KF.processNoiseCov, Scalar::all(1e-4));
	setIdentity(KF.measurementNoiseCov, Scalar::all(1e-2));
	setIdentity(KF.errorCovPost, Scalar::all(.1));
}


Mykalman::~Mykalman(void)
{
}

void Mykalman::Predict()
{
	prediction = KF.predict();
}

void Mykalman::Correct()
{
	this->estimated = KF.correct(this->measurement);
}

Mat_<float> Mykalman::GetState()
{
	return this->state;
}

float Mykalman::GetElemPrediction_i(int i)
{
	return prediction.at<float>(i);
}

float Mykalman::GetElemEstimated_i(int i)
{
	return this->estimated.at<float>(i);
}

void Mykalman::SetMeasurement(float a, float b)
{
	measurement.at<float>(0) = a;
	measurement.at<float>(1) = b;
}