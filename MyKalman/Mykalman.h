#pragma once
#include <iostream>
#include <vector>

#include<opencv2\opencv.hpp>
//#include <opencv2/opencv.hpp>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/video/tracking.hpp>
//#include <opencv2/highgui/highgui_c.h>

using namespace cv;
using namespace std;

class Mykalman
{
private:
	
	Mat_<float> state;
	Mat processNoise;
	Mat_<float> measurement;
	Mat prediction;
	Mat estimated;
	KalmanFilter KF;
public:
	Mykalman(void);
	~Mykalman(void);
	void Predict();
	void Correct();
	float GetElemPrediction_i(int i);
	float GetElemEstimated_i(int i);
	void SetMeasurement(float, float);
	Mat_<float> GetState();
};